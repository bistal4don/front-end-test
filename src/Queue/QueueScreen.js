import React, { Component } from "react";
import { ListGroup, Form } from "react-bootstrap";
import moment from "moment";
import md5 from "md5-hash";
import base64 from "base-64";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: [],
      filterString: "",
      time: Date.now()
    };
  }

  componentDidMount() {
    this.interval = setInterval(
      () => this.setState({ time: Date.now() }),
      30000
    );
    this.fetchCustomers();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  fetchCustomers() {
    const headers = new Headers();
    headers.set(
      "Authorization",
      "Basic " + base64.encode("codetest1:codetest100")
    );
    fetch("api/queue/gj9fs", {
      method: "GET",
      headers
    })
      .then(response => response.json())
      .then(json => {
        console.log("json ", json);
        this.setState({
          customers: json.queueData.queue.customersToday
        });
      });
  }

  filterCustomers = e => {
    const {
      target: { name, value }
    } = e;

    console.log("name ", value, name);

    this.setState({
      [name]: value
    });
  };

  render() {
    const { customers, filterString, time } = this.state;

    console.log("I updated at ", moment(time).format("h:mm:ss a"));

    return (
      <React.Fragment>
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Search</Form.Label>
            <Form.Control
              type="input"
              placeholder="Enter name"
              value={filterString}
              name="filterString"
              onChange={this.filterCustomers}
            />
            <Form.Text className="text-muted">
              Filter by cutomers name, start my typing a letter
            </Form.Text>
          </Form.Group>
        </Form>

        <ListGroup>
          {customers &&
            customers.map(index => {
              let imgHash = index.customer.emailAddress;
              if (imgHash)
                imgHash = md5(index.customer.emailAddress.trim().toLowerCase());

              if (
                index.customer.name
                  .toLowerCase()
                  .includes(filterString.toLowerCase())
              ) {
                return (
                  <ListGroup.Item key={index.id}>
                    <img
                      alt="img"
                      src={`https://www.gravatar.com/avatar/{imgHash}?s=50`}
                    />
                    <span style={{ paddingLeft: "5%" }}>
                      {index.customer.name}
                    </span>

                    <div
                      style={{
                        float: "right"
                      }}
                    >
                      {moment(index.expectedTime).format(
                        "MMMM Do YYYY, h:mm:ss a"
                      )}
                    </div>
                  </ListGroup.Item>
                );
              }
            })}
        </ListGroup>
      </React.Fragment>
    );
  }
}
